import java.io.File

import Functions._
import Traversal.Element
import org.apache.commons.io.FileUtils
import reader.Reader
import ujson._

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.io.Source

/*
val productAsJson = Reader.readFile(path = "D:\\Projects\\APP\\app-managed-product\\src\\main\\resources\\samples\\6.json").trim
 */

object TraverseToSeqApp extends App {

  val input = "d:\\Dropbox\\tqsm\\2019-08\\tesco\\raw2019-07-16_2019-07-17 "
  val output = "D:\\tmp\\"

  for (productAsJson <- Source.fromFile(input).getLines) {
    println(s"Processing product: ${productAsJson.take(50)}")
    val product = Product(Traversal(productAsJson))
    write(product)
  }

  def dir(path: String): File = new File(output + path)

  def write(product: Product): Unit = {
    product.productAttributes.foreach { element: Element =>
      FileUtils.writeStringToFile(
        dir(element.path),
        s"VALUE: ${element.value.take(70).padTo(70, " ").mkString}  ${product.line}\n",
        "UTF-8",
        true)
    }
  }
}

case class Product(tpnb: String, gtin: String, description: String, productAttributes: Seq[Element]) {
  lazy val line = s"TPNB: ${tpnb.padTo(15, " ").mkString} GTIN: ${gtin.padTo(20, " ").mkString} DESC: $description"
  override def toString: String =
    s"""Product:
       |  tpnb       :$tpnb
       |  gtin       :$gtin
       |  description:$description
       |${productAttributes.mkString("\n")}
       |""".stripMargin
}

object Product {
  def apply(elements: Seq[Element]): Product = {
    val tpnb: String = elements.collectFirst {
      case Element(Seq("tpnb"), tpnb) => tpnb
    }.getOrElse("null")
    val gtin: String = elements.collectFirst {
      case Element(Seq("gtin"), gtin) => gtin
    }.getOrElse("null")
    val description: String = elements.collectFirst {
      case Element(Seq("description"), description) => description
    }.getOrElse("null")
    Product(
      tpnb,
      gtin,
      description,
      elements
        .filter(onlyProductAttributes)
        .filter(removeNames)
        .map(stripValueKey))
  }
}

object Functions {
  val onlyProductAttributes: Element => Boolean = element =>
    element.paths match {
      case Seq("productAttributes", _*) => true
      case _ => false
    }
  val removeNames: Element => Boolean = element =>
    element.paths match {
      case _ :+ last if last == "name" => false
      case _ => true
    }
  val stripValueKey: Element => Element = element =>
    element.paths match {
      case head :+ last if last == "value" => Element(head, element.value)
      case _ => element
    }
}

object Path {
  def path(parts: Seq[String]): String = s"${parts.mkString("/").padTo(80, " ").mkString}"
}

object Traversal {

  case class Element(paths: Seq[String], value: String) {
    lazy val path: String = Path.path(paths)
    override def toString: String =
      s"${Path.path(paths).padTo(80, " ").mkString}" +
        s"${value.replace("\n", " ")}"
  }

  def apply(jsonAsText: String): Seq[Element] = {
    val json: Value = ujson.read(jsonAsText)
    traverse(json)
  }

  def traverse(json: Value, path: Seq[String] = Seq.empty, acc: Seq[Element] = Seq.empty): Seq[Element] = json match {
    case Obj(pairs: mutable.LinkedHashMap[String, Value]) =>
      pairs.flatMap { case (key: String, json: Value) =>
        traverse(json, path :+ key, acc)
      }.toSeq
    case Arr(array: ArrayBuffer[Value]) => array.flatMap((value: Value) => traverse(value, path, acc))
    case Str(value: String) => acc :+ Element(path, value.replace("\n", " "))
    case Num(value: Double) => acc :+ Element(path, value.toString)
    case Bool(value: Boolean) => acc :+ Element(path, value.toString)
  }
}