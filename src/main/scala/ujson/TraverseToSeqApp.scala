package ujson

import reader.Reader

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object TraverseToSeqApp extends App {

  val jsonString: String =
    """
      |{
      |  "order": {
      |    "customer": {
      |      "name": "Custy McCustomer",
      |      "contactDetails": {
      |        "address": "1 Fake Street, London, England",
      |        "phone": "0123-456-789"
      |      }
      |    },
      |    "items": [{
      |      "id": 123,
      |      "description": "banana",
      |      "quantity": 1
      |    }, {
      |      "id": 456,
      |      "description": "apple",
      |      "quantity": 2
      |    }],
      |    "total": 123.45
      |  }
      |}
      |""".stripMargin

  val str =
    """{
      |"name":"John",
      |"age":30,
      |"cars":[ "Ford", "BMW", "Fiat" ]
      |}""".stripMargin

  val txt = Reader.readFile(path = "D:\\Projects\\APP\\app-managed-product\\src\\main\\resources\\samples\\6.json").trim

  val json: Value = ujson.read(txt)

  case class Element(val path: Seq[String], val value: String) {
    override def toString: String =
      s"${path.mkString("/").padTo(80, " ").mkString}" +
        s"${value.replace("\n", " ")}"
  }

  def traverse(json: Value, path: Seq[String] = Seq.empty, acc: Seq[Element] = Seq.empty): Seq[Element] = json match {
    case Obj(pairs: mutable.LinkedHashMap[String, Value]) =>
      pairs.flatMap { case (key: String, json: Value) =>
        traverse(json, path :+ key, acc)
      }.toSeq
    case Arr(array: ArrayBuffer[Value]) =>
      array.zipWithIndex.flatMap { case (value: Value, index: Int) =>
        traverse(value, path :+ index.toString, acc)
      }
    case Str(value: String) => acc :+ Element(path, value)
    case Num(value: Double) => acc :+ Element(path, value.toString)
    case Bool(value: Boolean) => acc :+ Element(path, value.toString)
  }

  traverse(json)
    .filter((element: Element) => element.path match {
      case Seq("productAttributes", _*) => true
      case _ => false
    })
    .foreach(println)

}