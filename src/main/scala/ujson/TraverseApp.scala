package ujson


import reader.Reader

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object TraverseApp extends App {

  val jsonString: String =
    """
      |{
      |  "order": {
      |    "customer": {
      |      "name": "Custy McCustomer",
      |      "contactDetails": {
      |        "address": "1 Fake Street, London, England",
      |        "phone": "0123-456-789"
      |      }
      |    },
      |    "items": [{
      |      "id": 123,
      |      "description": "banana",
      |      "quantity": 1
      |    }, {
      |      "id": 456,
      |      "description": "apple",
      |      "quantity": 2
      |    }],
      |    "total": 123.45
      |  }
      |}
      |""".stripMargin

  val str =
    """{
      |"name":"John",
      |"age":30,
      |"cars":[ "Ford", "BMW", "Fiat" ]
      |}""".stripMargin

  val txt = Reader.readFile(path = "D:\\Projects\\APP\\app-managed-product\\src\\main\\resources\\samples\\6.json").trim

  val json: Value = ujson.read(txt)

  def traverse(json: Value): Unit = json match {
    case Str(value: String) => println(s"Str: $value")
    case Obj(map: mutable.LinkedHashMap[String, Value]) =>
      println(s"Obj: $map")
      map.foreach { case (key: String, json: Value) => traverse(json) }
    case Arr(array: ArrayBuffer[Value]) =>
      println(s"Arr: $array")
      array.foreach((value: Value) => traverse(value))
    case Num(value: Double) => println(s"Num: $value")
    case Bool(value: Boolean) => println(s"Bool: $value")
  }

  traverse(json)

  //  println("Values : " + obj.values.head)

}