package reader

import scala.io.Source

object Reader {

  def readResource(path: String) = Source
    .fromInputStream(getClass.getResourceAsStream(path))("UTF-8")
    .getLines().mkString

  def readFile(path: String) = Source.fromFile(path)("UTF-8")
    .getLines().mkString

}
