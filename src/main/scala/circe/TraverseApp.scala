package circe

import io.circe.parser.parse
import io.circe.{Json, JsonObject}

import scala.annotation.tailrec

object TraverseApp extends App {

  val raw =
    """{
      |	"key_1": "value_1",
      |	"key_2": {
      |		"key_3": "value_3",
      |		"key_4 ": "value_4"
      |	},
      |  "key_5" : [5, null, { "key_6": "value_6"}]
      |}""".stripMargin

  def reckeys(json: Json): Vector[String] = {
    @tailrec def loop(todo: Vector[Json], found: Vector[String]): Vector[String] = {
      todo.headOption match {
        case Some(head: Json) if head.isObject => {
          head.asObject match {
            case None => loop(todo.tail, found)
            case Some(jObject: JsonObject) => {
              val (keys: Vector[String], values: Vector[Json]) = jObject.toVector.unzip
              loop(values ++ todo.tail, found ++ keys)
            }
          }
        }
        case Some(head: Json) if head.isArray => {
          head.asArray match {
            case None => loop(todo.tail, found)
            case Some(jArray: Vector[Json]) => {
              val values = jArray.toVector
              loop(values ++ todo.tail, found)
            }
          }
        }
        case Some(_) => loop(todo.tail, found)
        case None => found
      }
    }

    loop(Vector(json), Vector.empty)
  }

  parse(raw).map(reckeys).map(_.foreach(println))

}
