package circe.caseclasses

sealed trait Level
case object Junior extends Level
case object Regular extends Level
case object Senior extends Level

case class Member(firstName: String, lastName: String, level: Level)
case class Group(name: String, members: Seq[Member])

object Decoders {
  import io.circe.Decoder
  import io.circe.generic.semiauto._
  implicit val levelDecoder: Decoder[Level] = deriveDecoder
  implicit val itemDecoder: Decoder[Member] = deriveDecoder
  implicit val repetitionDecoder: Decoder[Group] = deriveDecoder
}

object Encoders {
  import io.circe.Encoder
  import io.circe.generic.semiauto._
  implicit val levelEncoder: Encoder[Level] = deriveEncoder
  implicit val idemEncoder: Encoder[Member] = deriveEncoder
  implicit val repetitionEncoder: Encoder[Group] = deriveEncoder
}

object CirceApp extends App {

  val group = Group("The A-Team", List(
    Member("John 'Hannibal'", "Smith", Senior),
    Member("Templeton 'Face(man)'", "Peck", Regular),
    Member("Captain H.M. 'Howling Mad'", "Murdock", Junior)
  ))

  import Encoders._
  import io.circe.syntax._
  val circeJson: io.circe.Json = group.asJson
  val json = circeJson.toString
  println(s"encoded: $json")

  import Decoders._
  import io.circe.parser.decode
  val instance: Either[io.circe.Error, Group] = decode[Group](json)
  println(s"decoded: $instance")

}
