package circe.caseclasses

case class Person(firstName: String, secondDame: String)

case class Finances(income: Double, costs: Double, interestRate: Double)

case class Data(persons: Seq[(Person, Finances)])

object GenericAuto extends App {

  val data = Data(
    Seq(
      (Person("Jon", "Smith"), Finances(1200, 800, 6.5)),
      (Person("Andrew", "Basic"), Finances(800, 800, 6.5))
    )
  )

  println(data)

  import io.circe.generic.auto._
  import io.circe.syntax._

  val prettyJson = data.asJson.spaces2

  println(prettyJson)

}
