package circe.cursors

import io.circe.ACursor
import io.circe.parser.parse

object CursorTraversal extends App {

  val jsonString: String =
    """
      |{
      |  "order":{
      |    "customer":{
      |      "name":"Custy McCustomer",
      |      "contactDetails":{
      |        "address":"1 Fake Street, London, England",
      |        "phone":"0123-456-789"
      |      },
      |      "age":500,
      |      "income":0,
      |      "sex":"fat"
      |    },
      |    "items":[
      |      {
      |        "id":123,
      |        "description":"banana",
      |        "quantity":1
      |      },
      |      {
      |        "id":456,
      |        "description":"apple",
      |        "quantity":2
      |      }
      |    ],
      |    "total":123.45
      |  }
      |}
      |""".stripMargin

  parse(jsonString) match {
    case Right(json) =>
      val cursor: ACursor = json.hcursor.downField("order").downField("customer")
      cursor.keys match {
        case Some(keys: Iterable[String]) =>
          keys.toSeq.foreach { key: String =>
            println(key)
          }
        case None => println("There are no keys")
      }
      println("------")
      println(cursor.values)
      traverse(cursor).foreach(println)
    case Left(error) =>
      println(error)
  }

  def traverse(cursor: ACursor, path: String = "", acc: Seq[String] = Seq.empty): Seq[String] = {
    cursor.keys match {
      case Some(keys) =>
        keys.map { key: String =>
          val keyPath: String = s"$path/$key"
          println(cursor.downField(key).focus.map(_.isArray))
          traverse(cursor.downField(key), keyPath, acc)
        }.flatten.toSeq
      case None =>
        acc :+ path
    }
  }
}
