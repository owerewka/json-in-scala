package circe.cursors

import io.circe.Decoder.Result
import io.circe.parser.parse
import io.circe.{ACursor, Json}

object Cursor extends App {

  val jsonString: String =
    """
      |{
      |  "order": {
      |    "customer": {
      |      "name": "Custy McCustomer",
      |      "contactDetails": {
      |        "address": "1 Fake Street, London, England",
      |        "phone": "0123-456-789"
      |      }
      |    },
      |    "items": [{
      |      "id": 123,
      |      "description": "banana",
      |      "quantity": 1
      |    }, {
      |      "id": 456,
      |      "description": "apple",
      |      "quantity": 2
      |    }],
      |    "total": 123.45
      |  }
      |}
      |""".stripMargin


  val json: Json = parse(jsonString).getOrElse(Json.Null)

  val cursor: ACursor = json
    .hcursor
    .downField("order")
    .downField("customer")
    .downField("contactDetails")
  println(cursor.focus)

  val maybeAddress: Result[String] = cursor.get[String]("address")
  maybeAddress match {
    case Left(failure) => println(failure)
    case Right(address) => println(address)
  }
  cursor.get[String]("phone") match {
    case Left(failure) => println(failure)
    case Right(phone) => println(phone)
  }

}
