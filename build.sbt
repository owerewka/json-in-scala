name := "Json in Scala"

version := "0.1"

val circeVersion = "0.9.2"
scalaVersion := "2.12.4"

libraryDependencies ++= Seq(
  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser" % circeVersion,
  "io.circe" %% "circe-optics" % circeVersion,
  "com.lihaoyi" %% "ujson" % "0.7.5",
  "commons-io" % "commons-io" % "2.6"
)